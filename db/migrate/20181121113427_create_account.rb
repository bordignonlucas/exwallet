class CreateAccount < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.references :user, foreign_key: true, index: true
      t.datetime :inactive_at
    end
  end
end
