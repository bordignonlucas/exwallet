class CreateAccountActivity < ActiveRecord::Migration[5.2]
  def change
    create_table :account_activities do |t|
      t.references :account, foreign_key: true, index: true
      t.integer :activity_type
      t.float :amount

      t.timestamps
    end
  end
end
