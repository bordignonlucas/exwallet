require 'test_helper'

describe Accounts::Creator do
  subject { described_class.new(user) }

  let(:user) { create :user }

  context 'success' do
    it 'creates account' do
      expect(subject.create).to be_truthy
      expect(Account.count).to eq(1)
      expect(Account.last).to have_attributes(user_id: user.id)
    end
  end

  context 'failure' do
    it 'already has an account' do
      expect(subject.create).to be_truthy
      expect(subject.create).to be_falsey
      expect(subject.errors.full_messages)
        .to eq(['User already has a bank account.'])
    end
  end
end
