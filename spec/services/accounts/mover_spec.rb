require 'test_helper'

describe Accounts::Mover do
  subject { described_class.new(sender) }

  let(:sender_user) { create :user, email: 'sender@mail.com' }
  let(:sender) { create :account, user: sender_user }
  let(:receiver_user) { create :user, email: 'receiver@mail.com' }
  let(:receiver) { create :account, user: receiver_user }

  context 'success' do
    before do
      create :account_activity, account: sender,
                                amount: 200,
                                activity_type: :credit
    end

    it 'transfer' do
      expect(subject.transfer(receiver, amount: 150)).to be_truthy

      expect(sender.balance).to eq(50)
      expect(receiver.balance).to eq(150)
      expect(AccountActivity.count).to eq(3)
    end
  end

  context 'failure' do
    context 'invalid account - sender' do
      let(:sender) { nil }

      it 'raise error' do
        expect { subject.transfer!(receiver, amount: 20) }
          .to raise_error(Accounts::EmptyAccountError)
      end
    end

    context 'invalid account - receiver' do
      let(:receiver) { nil }

      it 'raise error' do
        expect { subject.transfer!(receiver, amount: 20) }
          .to raise_error(Accounts::EmptyAccountError)
      end
    end

    context 'not enough credits' do
      it 'raise error' do
        expect { subject.transfer!(receiver, amount: 20) }
          .to raise_error(Accounts::InsuficientBalance)

        expect(AccountActivity.count).to eq(0)
        expect(sender.balance).to eq(0)
        expect(receiver.balance).to eq(0)
      end
    end
  end
end
