require 'test_helper'

describe User, type: :model do
  context 'validations' do
    it { should validate_presence_of(:email) }
  end
end
