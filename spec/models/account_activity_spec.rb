require 'test_helper'

describe AccountActivity, type: :model do
  context 'validations' do
    it { should validate_presence_of(:account) }
    it { should validate_presence_of(:activity_type) }
    it { should validate_numericality_of(:amount).is_greater_than(0) }
  end
end
