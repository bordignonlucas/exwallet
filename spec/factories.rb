FactoryBot.define do
  factory :user do
    email { 'testing@mail.com' }
    cpf { '999.999.999-88' }
    password { '123qwe' }
  end

  factory :account do
    user { association(:user) }
  end

  factory :account_activity do
    account { association(:account) }
    amount 199.9
    activity_type :credit
  end
end
