ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'

require 'rails/test_help'

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.syntax = :expect
  end

  config.order = :random

  config.before(:suite) do
    DatabaseCleaner.clean_with(:deletion)
  end

  config.before(:each) do
    DatabaseCleaner.strategy = :deletion
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end

  config.include FactoryBot::Syntax::Methods
  config.before(:suite) do
    FactoryBot.find_definitions
  end

	Shoulda::Matchers.configure do |config|
		config.integrate do |with|
			with.test_framework :rspec
			with.library :rails
		end
	end
end

ActiveRecord::Migration.maintain_test_schema!
