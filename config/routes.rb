Rails.application.routes.draw do
  devise_for :users

  root 'home#index'

  resources :users, only: %i[index]
  resources :accounts, only: %i[index] do
    member do
      post :transfer
    end
  end
end
