module Accounts
  class EmptyAccountError < StandardError; end
  class InsuficientBalance < StandardError; end
  # Transfers money between accounts
  class Mover
    include ActiveModel::Validations

    attr_reader :sender_account

    validates :sender_account, presence: true

    def initialize(sender_account)
      @sender_account = sender_account
    end

    def transfer(receiver_account, amount:)
      transfer!(receiver_account, amount: amount)
    rescue ActiveRecord::RecordInvalid,
           ActiveModel::ValidationError,
           EmptyAccountError,
           InsuficientBalance
      nil
    end

    def transfer!(receiver_account, amount:)
      raise EmptyAccountError if sender_account.blank? ||
                                 receiver_account.blank?

      Account.transaction do
        raise InsuficientBalance unless can_transfer?(amount)

        AccountActivity.create!(account: sender_account,
                                activity_type: :debit,
                                amount: amount)

        AccountActivity.create!(account: receiver_account,
                                activity_type: :credit,
                                amount: amount)
      end
    end

    private

    def can_transfer?(amount)
      sender_account.balance >= amount
    end
  end
end
