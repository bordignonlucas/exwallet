module Accounts
  # Creates a new bank account
  class Creator
    include ActiveModel::Validations

    attr_reader :user

    validates :user, presence: true
    validate :user_does_not_have_account

    def initialize(user)
      @user = user
    end

    def create
      create!
    rescue ActiveRecord::RecordInvalid,
           ActiveModel::ValidationError
      nil
    end

    def create!
      validate!

      Account.create!(user: user)
    end

    private

    def user_does_not_have_account
      return unless Account.where(user: user).exists?

      errors.add(:base, 'User already has a bank account.')
    end
  end
end
