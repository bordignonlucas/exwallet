# Entity that represents a bank account
class Account < ApplicationRecord
  belongs_to :user

  has_many :account_activities

  validates :user, presence: true

  def balance
    credits = account_activities.credit.sum(:amount)
    debits = account_activities.debit.sum(:amount)

    credits - debits
  end
end
