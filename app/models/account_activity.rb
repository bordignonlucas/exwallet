# Entity to register credits and debits
class AccountActivity < ApplicationRecord
  belongs_to :account

  enum activity_type: %i[credit debit]

  validates :account, :activity_type, presence: true
  validates :amount, numericality: { greater_than: 0 }, presence: true
end
