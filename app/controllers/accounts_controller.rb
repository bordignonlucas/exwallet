# Accounts controller
class AccountsController < ApplicationController
  def index
    @account = Account.find_by(user_id: current_user.id)
  end

  def transfer; end
end
